<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css" />

    <title>Login Pasien</title>
  </head>
  <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="Index">MENTAL HEALTH CONSULTANT</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarColor01">
                  <ul class="navbar-nav mr-auto">
                
                  </ul>
                  <ul class="form-inline navbar-nav">
                        <li class="nav-item">
                                <a class="nav-link">Welcome,  Putri Nabila</a>
                             </li>
               
                        <li class="nav-item">
                                <a class="nav-link" href="Index">Log out</a>
                        </li>
                        
                    </ul>
        
                </div>
              </nav>
<br>
<br>
<br>


<div class="container">
    <div class="row">
        <div class="col-8">
                <table class="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Name of Consultant</th>
                            <th scope="col">Contact</th>
                            <th scope="col">Fee</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="table-active">
                            <th scope="row">1</th>
                            <td> Falah Rizki M.Psi., Psikolog</td>
                            <td> Fariz123@gmail.com</td>
                            <td> Rp50.000/jam</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td> Tyko Zidane M.Psi., Psikolog</td>
                            <td> TyokSangPenenang@gmail.com</td>
                            <td> Rp60.000/jam </td>
                          </tr>
                          <tr class="table-active">
                            <th scope="row">3</th>
                            <td>Bandana Irmal Abdillah MM.Psi., Psikolog</td>
                            <td>Bia234@gmail.com</td>
                            <td>Rp70.000/jam</td>
                          </tr>
                           <tr>
                            <th scope="row">4</th>
                            <td>Irfan Satrio Nugroho M.Psi., Psikolog</td>
                            <td>BangsatPsikologi@gmail.com</td>
                            <td>Rp55.000/jam</td>
                          </tr>
                        <tr class="table-active">
                            <th scope="row">3</th>
                            <td>Daniel Rama MMM.Psi., Psikolog</td>
                            <td>DanielMarutu@gmail.com</td>
                            <td>Rp100.000/jam</td>
                          </tr>
                        </tbody>
                      </table> 
        </div>
        <div class="col-4">

                <div class="card mb-3">
                        <h3 class="card-header">Profil Pasien</h3>
                        <div class="card-body">
                        </div>
                        <img style="height: 100%; width: 100%; display: block;" src="<?php echo base_url();?>assets/img/pasien1.jpg">
                        <div class="card-body">
                        </div>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Putri Nabila</li>
                          <li class="list-group-item">Reung-Reung Kec. Kembang Tanjong</li>
                          <li class="list-group-item">putrinabila@gmail.com</li>
                          <li class="list-group-item">085260126004</li>
                        </ul>
                </div>       
        </div>
    </div>

</div>

<div>

        <br>
        <br>
        <footer class=" card text-white bg-primary">
        
             <br>
                <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
            <br>
            </footer>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  </body>
</html>