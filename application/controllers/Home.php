<?php

class Home extends CI_Controller{
    /*public function __contruct(){
        parent::__construct();
        $this->load->model('model_user');
    }*/

    public function index(){
        $this->load->view('beranda');
    }

    public function LoginKonsultan(){
        $this->load->view('LoginAdmin');
    }

    public function LoginPasien(){
        $this->load->view('LoginPasien');
    }

    public function Pasien(){
        $this->load->view('Pasien');
    }

    public function Admin(){
        $this->load->view('Admin');
    }
    public function RegisterPasien(){
        $this->load->view('RegisterPasien');
    }
    public function AboutUs(){
        $this->load->view('AboutUs');
    }

}